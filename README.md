# Regolamento Penocchio

![build status](https://gitlab.com/1998marcom/regolamento-penocchio/badges/master/pipeline.svg)

Repo per il vecchio regolamento del torneo E. Penocchio, torneo di calcetto
della SNS. In realtà ci sono alcune differenze e alcune regole a voce che sono
state formalizzate e che non c'erano nel vecchio PDF del 2017.

Regolamento disponibile [qui](https://gitlab.com/1998marcom/regolamento-penocchio/-/jobs/artifacts/master/raw/regolamento.pdf?job=build).

![CC-BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "CC-BY-SA")
